import mongoose from 'mongoose';

const Schema = mongoose.Schema;

// create a schema
const cardSchema = new Schema({
  // You may need to add other fields like owner
  title: String,
  categories: String,
  inputs: Array ,
  outputs: Array ,
  image: {data: Buffer, contentType: String},
});
const CardBuffer = mongoose.model('card', cardSchema);
export default CardBuffer;
