import express from 'express'
import mongoose from 'mongoose'
import bodyParser from 'body-parser'
import multer from 'multer'

import Card from './models/CardModelBuffer'

var uploadBuffer = multer({ storage: multer.memoryStorage()})

const port = process.env.PORT || 8081
const server = express()

server.use(bodyParser.urlencoded({
  extended: true
}))
server.use(bodyParser.json())


mongoose.connect('mongodb://userDb:1234abc@ds137600.mlab.com:37600/cardsdb')

let router = express.Router()

router.use('/', express.static((__dirname + './../../')))

router.route('/api/cards')
  .get((req, res) => {
    Card.find((err, cards) => {
      if (err) {
        res.send(err)
      }
      res.send(cards)
    })
  })
  .post(uploadBuffer.single('photo'), (req, res) => {
    let card = new Card()
    card.title = req.body.title
    card.categories = req.body.categories
    card.image = {
      data: req.file.buffer,
      contentType: req.file.mimetype
    }
    card.inputs = req.body.inputs
    card.outputs = req.body.outputs
    card.save((err) => {
      if (err) {
        res.send(err)
      }
      res.send({
        message: 'created',
        title: req.body.title
      })
      //res.send(card)
    })
  })
  .delete((req, res) => {
    Card.remove({}, (err) => {
      if (err) {
        res.send(err)
      }
    })
  })

router.route('/api/images/:card_id')
  .get((req, res) => {
    Card.findOne({
      _id: req.params.card_id
    }, (err, card) => {
      if (err) {
        res.send(err)
      } else {
        res.writeHead(200, {
          'Content-Type': card.image.contentType
        });
        res.end(card.image.data)
      }
    })
  })

router.route('/api/cards/:card_id')
  .put((req, res) => {
    card.findOne({
      _id: req.params.card_id
    }, (err, card) => {

      if (err) {
        res.send(err)
      }
      card.title = req.body.title
      card.categories = req.body.categories
      card.image = req.body.image
      card.save((err) => {
        if (err) {
          res.send(err)
        }
        res.send({
          message: 'card updated'
        })
      })
    })
  })
  .get((req, res) => {
    card.findOne({
      _id: req.params.card_id
    }, (err, card) => {
      if (err) {
        res.send(err)
      }
      res.send(card)
    })
  })
  .delete((req, res) => {
    card.remove({
      _id: req.params.card_id
    }, (err) => {
      if (err) {
        res.send(err)
      }
      res.send({
        message: 'card deleted'
      })
    })
  })


server.use('/', router)

server.listen(port, (err) => {
  console.log("mon serveur est en route sur le port ", port)
})
