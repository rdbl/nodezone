import React from 'react'
import './Card.sass'
import HandlePoint from '../HandlePoint'

class Card extends React.Component{

  state = {
    x: this.props.x,
    y: 150
  };

  handleMouseDown = (e) => {
    this.coords = {
      x: e.pageX,
      y: e.pageY
    }
    document.addEventListener('mousemove', this.handleMouseMove);
  };

  handleMouseUp = () => {
    document.removeEventListener('mousemove', this.handleMouseMove);
    this.coords = {};
  };

  handleMouseMove = (e) => {

    const xDiff =  this.coords.x - e.pageX;
    const yDiff =  this.coords.y - e.pageY;

    this.coords.x = e.pageX;
    this.coords.y = e.pageY;

    this.setState({
      x: this.state.x - xDiff,
      y: this.state.y - yDiff
    });
  };

  render(){

    const coord ={
      rectCard:{
        w:150,
        h:220
      },
    }
    var inputs
    var inputsProps = this.props.inputs
    if(inputsProps){
      inputs = inputsProps.map((input,key)=>(
        <HandlePoint key={(key+"_input")} id={(key+"_input")} hanldePointX="0" hanldePointY={50+(key*40)} />
      ))
    }
    var outputs
    var outputsProps = this.props.outputs
    if(outputsProps){
      outputs = outputsProps.map((output,key)=>(
          <HandlePoint key={(key+"_output")} id={(key+"_output")} hanldePointX="150" hanldePointY={50+(key*40)} />
      ))
    }


    return(

      <g transform={"translate("+this.state.x+","+this.state.y+")"} className='cardGroup' ref="masterTransform">

        <g transform={"translate(-"+coord.rectCard.w/2+",-"+coord.rectCard.h/2+")"} className='cardGroup' >

          <rect  className="rectCard" width={coord.rectCard.w} height={coord.rectCard.h} rx="15" ry="15"/>
          <rect  className="rectAvatar" x="20" y="20" width={coord.rectCard.w-40} height={coord.rectCard.h-80} rx="10" ry="10"/>
          <image className="avatar"     x="25" y="35"  href={"/api/images/"+this.props.id} width={coord.rectCard.w-50} height={coord.rectCard.h-110} alt={this.props.image}/>
          <rect  className="rectTitleCard" width={coord.rectCard.w-40} height={coord.rectCard.h-(coord.rectCard.h/100*90)} rx="10" ry="10" x="20" y="180"/>
          <text  className="titleCard" textAnchor="middle" x={coord.rectCard.w/2} y="198" >{this.props.title}</text>
          <rect  className="rectCardDraggleTarget" width={coord.rectCard.w} height={coord.rectCard.h} rx="15" ry="15" onMouseDown={this.handleMouseDown} onMouseUp={this.handleMouseUp}/>
          {inputs}
          {outputs}
        </g>

      </g>
    )
  }
}
export default Card
