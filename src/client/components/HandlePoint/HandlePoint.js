import React from 'react'
import './HandlePoint.sass'

class HandlePoint extends React.Component{

  state = {
    x: this.props.hanldePointX,
    y: this.props.hanldePointY,
    linkActive:0
  };

  handleMouseDown = (e) => {
    this.coords = {
      x: e.pageX,
      y: e.pageY
    }
    document.addEventListener('mousemove', this.handleMouseMove);
  }

  handleMouseUp = () => {
    document.removeEventListener('mousemove', this.handleMouseMove);
    this.coords = {};
    this.setState({linkActive:0})
  }

  handleMouseMove = (e) => {
    const xDiff =  this.coords.x - e.pageX;
    const yDiff =  this.coords.y - e.pageY;

    this.coords.x = e.pageX;
    this.coords.y = e.pageY;

    this.setState({
      linkActive:1,
      x: this.state.x - xDiff,
      y: this.state.y - yDiff
    })
  }

  render(){
    var link

    if(this.state.linkActive){
      link = ( <line x1={this.props.hanldePointX} y1={this.props.hanldePointY}  x2={this.state.x}  y2={this.state.y}   /> )
    }

    return(
      <g>
        {link}
        <circle
          key={this.props.id}
          r='7'
          cx={this.props.hanldePointX}
          cy={this.props.hanldePointY}
          onMouseDown={this.handleMouseDown}
          onMouseUp={this.handleMouseUp}
        />

      </g>
    )
  }
}
export default HandlePoint
