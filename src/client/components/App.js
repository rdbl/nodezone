import React from 'react'
import './app.scss'
import Card from './Card'
import Link from './Link'
import FormMessage from './FormMessage'
import request from 'superagent'

class App extends React.Component {

  constructor() {
    super()
    this.state = {listCards:[]}

  }

  componentWillMount(){
    this.getDatafromDB()
  }

  getDatafromDB(){
    let dbItems =[]
    var result = request
    .get('/api/cards')
    .end((err,res) => {
      if(err){
        console.log(err);
      }
      JSON.parse(res.text).map((i)=>{
            dbItems.push(i)
      })
      this.setState({listCards:dbItems})
    });
  }

  handleRefresh(){
    this.getDatafromDB()
  }

  render(){

    const listCards = this.state.listCards
    const Cards = listCards.map((card,key)=>(

      <Card
        key     = {card._id}
        id     = {card._id}
        image   = {card.image}
        title   = {card.title}
        inputs  = {card.inputs}
        outputs = {card.outputs}
        x={160*(key+1)} />

      ))
      const Links = listCards.map((line,key)=>(

        <Link key={line._id} x={160*(key+1)} y={160} x2={160*(key+1)} y2={300}/>

      ))



    return (
      <div className='mainDiv'>
          <FormMessage className ='formCards'refresh={this.handleRefresh.bind(this)}/>
          <svg className='mainSvg'>
            {Cards}
          </svg>

      </div>
    )
  }
}

export default App
