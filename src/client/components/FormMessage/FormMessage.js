import React, {Component} from 'react'
import request from 'superagent'
import './FormMessage.css'

class FormMessage extends Component {

  constructor(props) {
    super(props);
    // this.state = {
    //   title: '',
    //   categories:'',
    //   image: ''
    // };
    this.handleInputs = this.handleInputs.bind(this);
  }

  handleInputs(e) {
    if (e.target.files === null) {
      this.setState({
        [e.target.id]: e.target.value
      })
    } else {
      this.setState({
        [e.target.id]: e.target.files[0]
      })
    }
  }

  handleDeleteDB(event){
    event.preventDefault()
    request.delete('/api/cards')
    .end(function(err) {
      if (err) {
        console.error(err)
      }
    })
    this.props.refresh()
  }

  handleSubmitAddCard(event) {

    this.props.refresh()
    var inputs = [1,2,3]
    var photo = new FormData();
    photo.append('photo', this.refs.image.files[0]);
    request.post('/api/cards')
    .field('title', this.state.title)
    .field('categories', this.state.categories)
    .field('inputs', inputs)
    .field('outputs', inputs)
    .attach('photo', this.state.image, 'photoname')
    .end(function(err) {
      if (err) {
        console.error(err)
      }
    })
    event.preventDefault()
  }
  /* onSubmit={this.handleSubmitAddCard.bind(this)} */
  render() {
    return (

      <div id="addForm">


      <form onSubmit={this.handleSubmitAddCard.bind(this)}>

          <p id="i1">Titre :
            <input className ='field' id="title" onChange={(e) => this.handleInputs(e)}/>
          </p>

          <p id="i1">categorie :
            <input className ='field' id="categories" onChange={(e) => this.handleInputs(e)}/>
          </p>

          <p id="i1">Image :
            <input className ='field' ref='image' id="image" onChange={(e) => this.handleInputs(e)} type="file"/>
          </p>

        <button type='submit'>Envoyer !</button>
        <button onClick={this.handleDeleteDB.bind(this)}>supprimer la base !</button>

      </form>


    </div>);
  }

}

export default FormMessage;
