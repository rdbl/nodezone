import React, {Component} from 'react'
import './Link.css'

class Link extends Component {

  state = {
    x: this.props.x,
    y:this.props.y,
    x2: this.props.x2,
    y2:this.props.y2
  };

  render() {



    return (
      <g>
        <line x1={this.state.x} y1={this.state.y}  x2={this.state.x2}  y2={this.state.y2}   />
        <circle cx= {this.state.x} cy= {this.state.y} r= "10" />
        <circle cx= {this.state.x2} cy= {this.state.y2} r= "10" />
      </g>
    )
}

}

export default Link;
