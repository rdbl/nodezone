var webpack = require('webpack');
var path = require('path');

var BUILD_DIR = path.resolve(__dirname, './build');
var APP_DIR = path.resolve(__dirname, './src/client');
var SERVER_DIR = path.resolve(__dirname, './src/server');
process.env.BABEL_ENV = 'development';
process.env.NODE_ENV = 'development';

const config = [
    {
        name: 'client',
        entry: {
          main: APP_DIR + '/index.js',
        },
        // target: 'web', // by default
        output: {
          path: __dirname + '/dist/client',
          filename: 'bundle.js',
        },
        module: {
          rules: [
           {
             test: /(\.css|.scss|.sass)$/,
             use: [{
                 loader: "style-loader" // creates style nodes from JS strings
             }, {
                 loader: "css-loader" // translates CSS into CommonJS
             }, {
                 loader: "sass-loader" // compiles Sass to CSS
             }]
           },
           {
             test: /\.(jsx|js)?$/,
             use: [{
               loader: "babel-loader",
               options: {
                 cacheDirectory: true,
                 presets: ['react', 'env','react-app'] // Transpiles JSX and ES6
               }
             }]
           }
          ],

      }
    },
]

module.exports = config;
